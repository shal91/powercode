;'use strict';
(function($){
    $('#datepicker').datepicker({
        dateFormat : 'dd.mm.yy'
    });
    $('#timepicker').timepicker({
        timeFormat : 'h:i:s'
    });

    function getSerializeObj(array) {
        var obj = {};
        $.each(array, function(key,value) {
            obj[value.name] = value.value;
        });
        return obj;
    }

    // todolist plugin

    $.fn.todoList = function() {
        var self = $(this);
        !localStorage.getItem('tasks') && localStorage.setItem('tasks','{}');
        var obj = {
            getTasks : function() {
                var tasks = typeof JSON.parse(localStorage.getItem('tasks')) === 'object' ? JSON.parse(localStorage.getItem('tasks')) : {};
                return tasks;
            },
            setTasks : function(tasks) {
                localStorage.setItem('tasks',JSON.stringify(tasks));
                obj.buildTasks();
            },
            addTask : function(data) {
                var tasks = obj.getTasks();
                tasks[Date.now()] = data;
                obj.setTasks(tasks);
            },
            buildTasks : function(elem) {
                self.find('.single-task').each(function () {
                    $(this).remove();
                });
                var tasks = obj.getTasks(),array = [],str='';
                $.each(tasks, function(key,value) {
                    value.id = key;
                    array.push(value);
                });

                // sort by date and status

                array.sort(function(a,b){
                    return a.expireDate.slice(-4) !== b.expireDate.slice(-4)                //by year
                        ? a.expireDate.slice(-4) < b.expireDate.slice(-4) ? 0 : 1
                        : a.expireDate.slice(-7,-4) !== b.expireDate.slice(-7,-4)           //by month
                            ?a.expireDate.slice(-7,-4) < b.expireDate.slice(-7,-4)? 0 : 1
                            :a.expireDate.slice(0,2) !== b.expireDate.slice(0,2)            //by day
                                ?a.expireDate.slice(0,2) < b.expireDate.slice(0,2) ? 0 : 1
                                :a.expireTime.slice(0,2) !== b.expireTime(0,2)              //by hours
                                    ?a.expireTime.slice(0,2) < b.expireTime(0,2) ? 0 : 1
                                    :a.expireTime.slice(3,5) !== b.expireTime(3,5)          //by minutes
                                        ?a.expireTime.slice(3,5) < b.expireTime(3,5) ? 0 : 1
                                        :a.expireTime.slice(6) !== b.expireTime(6)          //by seconds
                                            ?a.expireTime.slice(6) < b.expireTime(6) ? 0 : 1
                                            : a.done !== b.done                             //by status
                                                ? !a.done ? 0 : 1
                                                :0;
                });

                // build tasks html-tree

                $.each(array, function(key,value) {
                    str += '<tr class="single-task" id="task-'+value.id+'">' +
                        '<td class="done">' +
                        '<input type="checkbox"'+(value.done && ' checked="checked"')+' class="task-checked"/>' +
                        '</td>' +
                        '<td class="text">'+value.text+'</td>' +
                        '<td class="expire-date">'+value.expireDate +' '+value.expireTime+'</td>' +
                        '<td class="remove"><i class="fa fa-remove remove-task"></i></td>' +
                        '</tr>';
                });
                self.append(str);
            },
            removeTask : function(taskId) {
                var tasks = obj.getTasks();
                delete tasks[taskId];
                obj.setTasks(tasks);
            },
            checkTask : function(taskId,check) {
                var tasks = obj.getTasks();
                tasks[taskId].done = check;
                obj.setTasks(tasks);
            }
        };
        obj.buildTasks();


        // add listeners to checkbox and delete button
        self
            .on('click.removeTask','.remove-task', function() {
                var id = $(this).closest('.single-task').attr('id').slice(5);
                obj.removeTask(id);
            })
            .on('change.checkTask', '.task-checked', function() {
                var id = $(this).closest('.single-task').attr('id').slice(5),
                    check = $(this).is(':checked');
                obj.checkTask(id,check);
            });
        return obj;
    };

    var toDoPlugin = $('#tasks-list').todoList();

    $('#add-task-form').on('submit', function(e) {
        e.preventDefault();
        var data = getSerializeObj($(this).serializeArray());
        if (data['task-text'].length > 6 && data['expire_date'] && data['expire_time']) {
            toDoPlugin.addTask({
                text : data['task-text'],
                done : data.done ? true : false,
                expireDate : data['expire_date'],
                expireTime : data['expire_time']
            });
            $(this)[0].reset();
        }
    });
})(jQuery);